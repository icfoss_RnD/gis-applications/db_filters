#!/usr/bin/python3
from dbconnect import *
from queries import *
from time import sleep
import requests
import json
import sys
import timeit

start = timeit.default_timer()

conn = connect() # db_connect
if str(conn) == "None":
    print('********************')
    print('Error in connection!')
else:
    cur = conn.cursor() # create a cursor
    for query in queries:
        cur.execute(query) # execute query
        rows = cur.fetchall() # get all results
        for row in rows:
            # print(row) # print one by one
            source_id = str(row[0])
            origin = str(row[1])
            dest_id = str(row[2])
            destination = str(row[3])
            exist_check_query = "select * from input_ways "
            exist_check_query += "where (source_id="+source_id+" and dest_id="+ dest_id 
            exist_check_query += ") or (source_id="+dest_id+" and dest_id="+ source_id +");";
            cur.execute(exist_check_query)
            if cur.fetchone() is not None:
                print("Data already exists!")
                continue
            url = "http://10.176.14.173:5000/trip/v1/walking/"
            url += origin + ";" + destination
            url += "?source=first&destination=last"
            url += "&geometries=geojson&roundtrip=false"
            # print(url)
            continue
            response = requests.get(url)
            resp_json = response.json()
            if len(resp_json['trips']) > 0:
                trip_geom = resp_json['trips'][0]['geometry']
                with open('./geojson/'+origin+'_to_'+destination+'.geojson', 'w') as outfile:
                    json.dump(trip_geom, outfile)
                distance_in_mtrs = resp_json['trips'][0]['distance']
                insert_query = "INSERT INTO input_ways (source_id,dest_id,distance_in_mtrs,route_geom) "
                insert_query += "values(" + source_id + "," + dest_id + "," + str(distance_in_mtrs) 
                insert_query += ",ST_GeomFromText(ST_AsText(ST_GeomFromGeoJSON('" + json.dumps(trip_geom) +"')),4326));"
                print(insert_query)
                cur.execute(insert_query)
                print("Updated rows: " + str(cur.rowcount))
                conn.commit()
            else:
                print(resp_json['error_message'])
            print('done')
            # sleep(2)
        print('')
    cur.close
    disconnect(conn)
stop = timeit.default_timer()
total_time = stop - start
mins, secs = divmod(total_time, 60)
hours, mins = divmod(mins, 60)

sys.stdout.write("Total running time: %d:%d:%d.\n" % (hours, mins, secs))