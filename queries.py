closest_10_points_set = "select a.ogc_fid as p1,CONCAT(a.longitude, ',', a.latitude) as p1_coords, b.ogc_fid as p2,CONCAT(b.longitude, ',', b.latitude) as p2_coords from buffered_assets a CROSS JOIN LATERAL (SELECT ogc_fid,longitude,latitude FROM buffered_assets closest_pt where a.ogc_fid <> ogc_fid ORDER BY a.wkb_geometry <-> closest_pt.wkb_geometry LIMIT 10) AS b;"

input_dataset = "select * from input_nodes"

queries = [
input_dataset,
]
