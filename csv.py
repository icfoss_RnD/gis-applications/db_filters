#!/usr/bin/python3
from time import sleep
import requests
import json
import csv

op = open('output/input_ways.csv','wb')
op_writer = csv.writer(op)
op_writer.writerow(['source_id','dest_id','distance_in_mtrs','route_geom'])
with open('input/input_nodes.csv') as input:
    for row in input:
        print(row) # print one by one
        source_id = str(row[0])
        origin = str(row[1])
        dest_id = str(row[2])
        destination = str(row[3])

        url = "https://maps.keralamvd.gov.in/osrm/trip/v1/walking/"
        url += origin + ";" + destination
        url += "?source=first&destination=last"
        url += "&geometries=geojson&roundtrip=false"
        # print(url)
        response = requests.get(url)
        resp_json = response.json()
        if len(resp_json['trips']) > 0:
            distance_in_mtrs = resp_json['trips'][0]['distance']
            trip_geom = resp_json['trips'][0]['geometry']
            op_row = [source_id,dest_id,str(distance_in_mtrs),json.dumps(trip_geom)]
            # with open('./geojson/'+origin+'_to_'+destination+'.geojson', 'w') as outfile:
            #     json.dump(trip_geom, outfile)
        else:
            print(resp_json['error_message'])
        # print('sleeping for 2 seconds')
        # sleep(2)
    print('')