# KFON project dev steps

```
​https://gitlab.com/icfoss_RnD/db_filters/tree/osrm_routing​
```

1. Get substation point and make a buffer of it with required radius (7km as of now)
2. Query and filter the nodes set intersecting with the buffer area and store as ​ **_buffered_assets_**
3. Filter the **_buffered_assets_** and make most unique pairs of source and dest nodes possible as **_input_nodes_** ​ ​with query
```
CREATE TABLE input_nodes AS 
SELECT 
a.ogc_fid AS p1,CONCAT(a.longitude, ',', a.latitude) AS p1_coords, 
b.ogc_fid AS p2,CONCAT(b.longitude, ',', b.latitude) AS p2_coords 
FROM buffered_assets AS a CROSS JOIN LATERAL 
(SELECT 
ogc_fid,longitude,latitude 
FROM buffered_assets AS closest_pt 
WHERE a.ogc_fid <> ogc_fid 
ORDER BY a.wkb_geometry <-> closest_pt.wkb_geometry 
LIMIT 10) AS b;
```
4. Run the python script for obtaining routes with distance from node to node and the LineString geometry from OSRM api of CDAC and store as **_input_ways_**  in DB
5. Filter distinct routes from each source and the Linestrings with shortest distance (weight) as **_shortest_routes_** ​using the query
```
CREATE TABLE shortest_routes AS 
SELECT 
DISTINCT ON (source_id) source_id,dest_id,distance_in_mtrs,route_geom
FROM input_ways 
WHERE distance_in_mtrs > 0 
ORDER BY source_id,distance_in_mtrs
```
6. Load the layers **_shortest_routes_** and **_buffered_assets_** into QGIS
7. Topology errors (connection in lines, duplications, overlapping lines, dangles, Angles in
    intersection etc.) in the line network is then corrected using the following methods:
   - Errors are found by running ​Topology checker ​algorithm based on rules.
   - Merge lines plugin for merging lines based on Length and Alignment.
   - Node extraction of the lines to identify vertices of the segments
   - The lines are then split into segments based on the nodes using ​ **_explode lines function_**
   - Snapping function is iteratively done with different thresholds to merge lines using v.clean ​tool in GRASS
8. The cleaned data is then merged using ​ **_dissolve function/multipart to single function_**
9. Duplicates in the data are removed based on their geometry using the ​ **_MMQGIS plugin_**
10. Total length is calculated using ​ **_field calculator_**